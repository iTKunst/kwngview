/**********************************************************************
 *
 * kwNgComp/img/kwcImg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from "@angular/core";

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtImg} from "./kwtImg";

// @formatter:on

const sTAG: string = "img";

@Component({
    selector: "kwc-img",
    templateUrl: "kwcImg.html",
    styleUrls: ["kwcImg.scss"]
})
export class kwcImg extends kwNgCtrlCompId {

    icon: object;
    logo: object;
    svg: object;
    svgRaw: object;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log(this.sClass, "::constructor() called.");
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtImg = <kwtImg>view;


        let icon: object = type.icon;
        if (kw.isValid(icon)) {
            //console.info(this.sClass, "::parseView() icon is [", icon, "]");
            this.icon = icon;
        }


        let logo: object = type.logo;
        if (kw.isValid(logo)) {
            //console.info(this.sClass, "::parseView() logo is [", logo, "]");
            this.logo = logo;
        }


        let svg: object = type.svg;
        if (kw.isValid(svg)) {
            //console.info(this.sClass, "::parseView() svg is [", svg, "]");
            this.svg = svg;
        }

        let svgRaw: object = type.svgRaw;
        if (kw.isValid(svgRaw)) {
            //console.info(this.sClass, "::parseView() svgRaw is [", svgRaw, "]");
            this.svgRaw = svgRaw;
        }

        if (kw.isNull(icon)
            && kw.isNull(logo)
            && kw.isNull(svg)
            && kw.isNull(svgRaw)) {
            const sMsg = "Only recognizes [icon, logo, svg, svgRaw]"
            console.error(this.sClass, "::parseView() ", sMsg);
        }

        if (kw.isNull(icon)
            && kw.isNull(logo)
            && kw.isNull(svg)
            && kw.isNull(svgRaw)) {
            const sMsg = "Requires one of [icon, logo, svg, svgRaw]"
            console.error(this.sClass, "::parseView() ", sMsg);
        }

    }
}
