/**********************************************************************
 *
 * kwNgComp/tabs/Link/kwcTabsLink.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtTabsLink} from "./kwtTabsLink";

const sTAG: string = "";


@Component({
    selector: 'kwc-tabs-Link',
    templateUrl: 'kwcTabsLink.html',
    styleUrls: ['kwcTabsLink.scss']
})
export class kwcTabsLink extends kwNgCtrlCompId {
    public sLink: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcTabsLink::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");

    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtTabsLink = <kwtTabsLink>view;


        let sLink: string = type.sLink;
        if (kw.isString(sLink)) {
            this.sLink = sLink;
            //console.info(this.sClass, "::parseView() sLink is [", sLink, "]");
        }

        let title: object = type.title;
        if (kw.isValid(title)) {
            //console.info(this.sClass, "::parseView() title is [", title, "]");
        }

        let img: object = type.img;
        if (kw.isValid(img)) {
            //console.info(this.sClass, "::parseView() img is [", img, "]");
        }

        if (kw.isNull(img)
            && kw.isNull(title)
            && !kw.isString(sLink)) {
            let sMsg = "Recognizes only [img, sLink, title]";
            console.error(this.sClass, "::parseView()", sMsg);
        }

        if (kw.isNull(img)
            && kw.isNull(title)) {
            let sMsg = "requires one of [img, title]";
            console.error(this.sClass, "::parseView() ", sMsg);
        }

        if (!kw.isString(sLink)) {
            let sMsg = "Absolutely requires [sLink]";
            console.error(this.sClass, "::parseView() ", sMsg);
        }

    }
}
