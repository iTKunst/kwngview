/**********************************************************************
 *
 * kwNgComp/tabs/title/kwcTabsTitle.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwLog} from "@kw/kwLog";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtTabsTitle} from "./kwtTabsTitle"

const sTAG: string = "title";


@Component({
    selector: 'kwc-TabsTitle',
    templateUrl: 'kwcTabsTitle.html',
    styleUrls: ['kwcTabsTitle.scss']
})
export class kwcTabsTitle extends kwNgCtrlCompId {
    public sTitle: string;
    public sSubTitle: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(
            sTAG,
            srvcDisp);

        //console.log('kwcTabsTitle::constructor() called.');
    }


// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        const log = new kwLog(this.sClass, "parseView");
        //console.info(log.called());

        if (kw.isNull(view)) {
            const sMsg = kw;
            console.error(log.invalid("view"));
            return;
        }

        let type: kwtTabsTitle = <kwtTabsTitle>view;

        let sTitle: string = type.sTitle;
        if (kw.isString(sTitle)) {
            //console.info(log.is("sTitle", sTitle));
            this.sTitle = sTitle
        }


        let sSubTitle: string = type.sSubTitle;
        if (kw.isString(sSubTitle)) {
            //console.info(log.is("sSubTitle", sSubTitle));
            this.sSubTitle = sSubTitle
        }


        if (!kw.isString(sTitle) && kw.isString(sSubTitle)) {
            console.error(log.recognizes("sTitle, sSubTitle"));
        }

        if (!kw.isString(sTitle)) {
            console.error(log.requires("sTitle"));
        }

    }

}
