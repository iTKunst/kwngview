/**********************************************************************
 *
 * kwNgComp/util/kwcUtil.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off

import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtUtil} from "./kwtUtil";


const sTAG: string = "util";

@Component({
    selector: 'kwc-util',
    templateUrl: 'kwcUtil.html',
    styleUrls: ['kwcUtil.scss']
})
export class kwcUtil extends kwNgCtrlCompId {

    public lft: object;
    public rht: object;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(
            sTAG,
            srvcDisp);
        //console.log('kwcUtil::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtUtil = <kwtUtil>view;

        let lft: object = type.lft;
        if (kw.isValid(lft)) {
            //console.info(this.sClass, "::parseView() lft is [", lft, "]");
            this.lft = lft;
        }


        let rht: object = type.rht;
        if (kw.isValid(rht)) {
            //console.info(this.sClass, "::parseView() rht is [", rht, "]");
            this.rht = rht;
        }


        if (kw.isNull(lft) && kw.isNull(rht)) {
            console.error(this.sClass, "::parseView() Only recognize [lft, rht]");
        }
    }

}
