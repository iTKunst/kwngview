// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtSvg} from "./kwtSvg";


const sTAG: string = "svg";


@Component({
    selector: 'kwc-svg',
    templateUrl: 'kwcSvg.html',
    styleUrls: ['kwcSvg.scss']
})
export class kwcSvg extends kwNgCtrlCompId {
    public sPath: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcSvg::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtSvg = <kwtSvg>view;


        let sPath: string = type.sPath;
        if (!kw.isString(sPath)) {
            //console.info(this.sClass, "::parseView() sPath is invalid");
            return;
        }
        //console.info(this.sClass, "::parseView() sPath is [", sPath, "]");
        this.sPath = sPath

    }

}
