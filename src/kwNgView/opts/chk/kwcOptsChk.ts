// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtOptsChk} from "./kwtOptsChk";


const sTAG: string = "";


@Component({
    selector: 'kwc-opts-chk',
    templateUrl: 'kwcOptsChk.html',
    styleUrls: ['kwcOptsChk.scss']
})
export class kwcOptsChk extends kwNgCtrlCompId {

    public sIcon: string;
    public sName: string;

    public bChecked: boolean = true;

    constructor(
        srvcDisp: kwBSDispVal) {
        super(sTAG, srvcDisp);
        //console.log('kwcOptsChk::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtOptsChk = <kwtOptsChk>view;

        let sText = this.sText;
        if (kw.isString(sText)) {
            //console.info(this.sClass, "::retrieve() sText is [",sText, "]");
        }

        let sIcon: string = type.sIcon;
        if (kw.isString(sIcon)) {
            //console.info(this.sClass, "::retrieve() sIcon is [", sIcon, "]");
            this.sIcon = sIcon;
        }

        if (!kw.isString(sIcon) && !kw.isString(this.sText)) {
            console.error(this.sClass, "::retrieve() Only recognizes [sIcon, sInitId, sText]");
        }

        if (!kw.isString(sIcon) && !kw.isString(this.sText)) {
            console.error(this.sClass, "::retrieve() At least 1 is required [ sIcon, sText]");
        }
    }
}
