/**********************************************************************
 *
 * kwNgComp/link/kwcLinkTag.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtLinkTag} from "./kwtLinkTag";

const sTAG: string = "link";


@Component({
    selector: 'kwc-link-tag',
    templateUrl: 'kwcLinkTag.html',
    styleUrls: ['kwcLinkTag.scss']
})
export class kwcLinkTag extends kwNgCtrlCompId {

    img: object;
    title: object;


    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcLinkTag::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        console.log(this.sClass, "::parseData() called.");

    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtLinkTag = <kwtLinkTag>view;

        let sLinkTmpl: string = this.getLinkTmpl();
        if (kw.isString(sLinkTmpl)) {
            //console.info(this.sClass, "::parseView() sLinkTmpl is [", sLinkTmpl, "]");
        }

        let sLink = this.getLink();
        if (kw.isString(sLink)) {
            //console.info(this.sClass, "::parseView() sLink is [", sLinkk, "]");
        }

        let sLinkId: string = this.getLinkId();
        if (kw.isString(sLinkId)) {
            //console.info(this.sClass, "::parseView() sLinkId is [", sLinkId, "]");
        }

        let title: object = type.title;
        if (kw.isValid(title)) {
            //console.info(this.sClass, "::parseView() title is [", title, "]");
            this.title = title;
        }

        let img: object = type.img;
        if (kw.isValid(img)) {
            //console.info(this.sClass, "::parseView() img is [", img, "]");
            this.img = img;
        }

        if (kw.isNull(sLinkId)
            && kw.isNull(img)
            && kw.isNull(title)
            && !kw.isString(sLink)
            && !kw.isString(sLinkTmpl)) {
            const sMsg = " Recognizes only [sLinkId, img, sLink, sLinkTmpl, title]";
            console.error(this.sClass, "::parseView() ", sMsg);
        }

        if (kw.isString(sLink)) {
            return;
        }

        if (!kw.isString(sLink)
            && !kw.isString(sLinkId)
            && !kw.isString(sLinkTmpl)) {
            const sMsg = "Requires one of [sLink, sLinkId, sLinkTmpl]";
            console.error(this.sClass, "::parseView() ", sMsg);
        }

        if (kw.isString(sLinkTmpl)
            && !kw.isString(sLinkId)) {
            const sMsg = "[sLinkTmpl] requires [sLinkId]";
            console.error(this.sClass, "::parseView() ", sMsg);
        }

        if (kw.isString(sLinkId)
            && !kw.isString(sLinkTmpl)) {
            const sMsg = "[sLinkId] may be used with [sLinkTmpl]";
            console.error(this.sClass, "::parseView() ", sMsg);
        }

    }

}
