/**********************************************************************
 *
 * kwNgComp/fadeIn/kwFadeIn.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {animate} from "@angular/animations";
import {style} from "@angular/animations";
import {transition} from "@angular/animations";
import {trigger} from "@angular/animations";
// @formatter:on


export let routeAnim = trigger('routeAnim', [
    transition('void => *', [
        style({
            opacity: 0,
        }),
        animate('400ms 150ms ease-in-out', style({
            opacity: 1,
        }))
    ]),
]);

export let fadeInAnim = trigger('fadeInAnim', [
    transition('void => *', [
        style({
            opacity: 0,
        }),
        animate('400ms 150ms ease-in-out', style({
            opacity: 1,
        }))
    ]),
]);
