/**********************************************************************
 *
 * kwNgComp/input/kwcInput.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off

import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtInput} from "./kwtInput";


const sTAG: string = "input";


@Component({
    selector: 'kwc-input',
    templateUrl: 'kwcInput.html',
    styleUrls: ['kwcInput.scss']
})
export class kwcInput extends kwNgCtrlCompId {

    sIcon: string;
    sPlaceholder: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcInput::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtInput = <kwtInput>view;


        let sIcon: string = type.sIcon;
        if (kw.isString(sIcon)) {
            //console.info(this.sClass, "::parseView() sIcon is [", sIcon, "]");
            this.sIcon = sIcon;
        }


        let sPlaceholder: string = type.sPlaceholder;
        if (kw.isString(sPlaceholder)) {
            //console.info(this.sClass, "::parseView() sPlaceholder is [", sPlaceholder, "]");
            this.sPlaceholder = sPlaceholder;
        }

        if (!kw.isString(this.sIcon) && !kw.isString(this.sPlaceholder)) {
            console.error(this.sClass, "::parseView() Only recognizes [sIcon, sPlaceholder]");
            console.error(this.sClass, "::parseView() Requires [ sPlaceholder]");
        }
    }
}
