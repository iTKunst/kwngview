/**********************************************************************
 *
 * kwNgComp/select/kwcSelect.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off

import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";

// @formatter:on


const sTAG: string = "select";


@Component({
    selector: 'kwc-select',
    templateUrl: 'kwcSelect.html',
    styleUrls: ['kwcSelect.scss']
})
export class kwcSelect extends kwNgCtrlCompId {
    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(
            sTAG,
            srvcDisp);

        //console.log('kwcSelect::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

    }

}
