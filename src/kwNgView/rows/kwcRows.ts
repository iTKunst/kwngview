/**********************************************************************
 *
 * kwNgComp/rows/kwcRows.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off

import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";


const sTAG: string = "rows";


@Component({
    selector: 'kwc-rows',
    templateUrl: 'kwcRows.html',
    styleUrls: ['kwcRows.scss']
})
export class kwcRows extends kwNgCtrlCompId {

    constructor(srvcDisp: kwBSDispVal) {
        super(sTAG, srvcDisp);
        //console.log(this.sClass, '::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");
    }

}
