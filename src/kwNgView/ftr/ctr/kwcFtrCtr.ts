/**********************************************************************
 *
 * kwNgComp/ftr/kwcFtrCtr.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from "@angular/core";

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtFtrCtr} from "./kwtFtrCtr";


const sTAG: string = "ctr";

@Component({
    selector: "kwc-ftr-ctr",
    templateUrl: "kwcFtrCtr.html",
    styleUrls: ["kwcFtrCtr.scss"]
})
export class kwcFtrCtr extends kwNgCtrlCompId {
    title: object;
    tabs: object;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log(this.sClass, "::constructor() called.");
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtFtrCtr = <kwtFtrCtr>this.view;

        let title: object = type.title;
        if (kw.isValid(title)) {
            //console.info(this.sClass, "::parseView() title is [", title, "]");
            this.title = title;
        }

        let tabs: object = type.tabs;
        if (kw.isValid(tabs)) {
            //console.info(this.sClass, "::parseView() tabs is [", tabs, "]");
            this.tabs = tabs;
        }

        if (kw.isNull(title) && kw.isNull(tabs)) {
            console.error(this.sClass, "::parseView() Only recognizes [title, tabs]");
        }

    }
}
