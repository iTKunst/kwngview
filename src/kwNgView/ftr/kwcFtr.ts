/**********************************************************************
 *
 * kwNgComp/ftr/kwcFtr.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from "@angular/core";

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtFtr} from "./kwtFtr";

// @formatter:on

const sTAG: string = "ftr";

@Component({
    selector: "kwc-ftr",
    templateUrl: "kwcFtr.html",
    styleUrls: ["kwcFtr.scss"]
})
export class kwcFtr extends kwNgCtrlCompId {

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log(this.sClass, "::constructor() called.");
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtFtr = <kwtFtr>view;

        let ctr: object = type.ctr;
        if (kw.isValid(ctr)) {
            //console.info(this.sClass, "::parseView() ctr is [", ctr, "]");
        }

        let lft: object = type.lft;
        if (kw.isValid(lft)) {
            //console.info(this.sClass, "::parseView() lft is [", lft, "]");
        }

        let rht: object = type.rht;
        if (kw.isValid(rht)) {
            //console.info(this.sClass, "::parseView() rht is [", rht, "]");
        }


        if (kw.isNull(ctr) && kw.isNull(lft) && kw.isNull(rht)) {
            console.error(this.sClass, "::parseView() Only recognizes [ctr, lft, rht]");
        }
    }
}
