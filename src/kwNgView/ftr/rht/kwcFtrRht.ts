/**********************************************************************
 *
 * kwNgComp/ftr/kwcFtrRht.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from "@angular/core";

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtFtrRht} from "./kwtFtrRht";

// @formatter:on

const sTAG: string = "rht";

@Component({
    selector: "kwc-ftr-rht",
    templateUrl: "kwcFtrRht.html",
    styleUrls: ["kwcFtrRht.scss"]
})
export class kwcFtrRht extends kwNgCtrlCompId {

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log(this.sClass, "::constructor() called.");
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtFtrRht = <kwtFtrRht>this.view;

        let btn: object = type.btn;
        if (kw.isValid(btn)) {
            //console.info(this.sClass, "::parseView() btn is [", btn, "]");
        }

        let img: object = type.img;
        if (kw.isValid(img)) {
            //console.info(this.sClass, "::parseView() img is [", img, "]");
        }

        let title: object = type.title;
        if (kw.isValid(title)) {
            //console.info(this.sClass, "::parseView() title is [", title, "]");
        }

        if (kw.isNull(img) && kw.isNull(title) && kw.isNull(btn)) {
            console.error(this.sClass, "::parseView() Only recognizes [img, title, btn]");
        }

    }
}
