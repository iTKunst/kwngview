/**********************************************************************
 *
 * kwNgComp/pageNotFound/kwcPageNotFound.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from '@angular/core';

// @formatter:on


@Component({
    selector: 'kwc-page-not-found',
    templateUrl: './kwcPageNotFound.html',
    styleUrls: ['./kwcPageNotFound.scss']
})
export class kwcPageNotFound {
    constructor() {
        //console.log('kwcPageNotFound::constructor() called.');
    }
}
