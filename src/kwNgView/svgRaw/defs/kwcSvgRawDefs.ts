// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtSvgRawDefs} from "./kwtSvgRawDefs";


const sTAG: string = "defs";


@Component({
    selector: 'kwc-svg-raw-defs',
    templateUrl: 'kwcSvgRawDefs.html',
    styleUrls: ['kwcSvgRawDefs.scss']
})
export class kwcSvgRawDefs extends kwNgCtrlCompId {

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcSvgRawDefs::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtSvgRawDefs = <kwtSvgRawDefs>this.view;

        let fltr: object = type.fltr;
        if (kw.isValid(fltr)) {
            //console.info(this.sClass, "::parseView() fltr is [", fltr, "]");
        }

        if (!kw.isValid(fltr)) {
            let sMsg = "Recognizes only [fltr]";
            console.error(this.sClass, "::parseView()", sMsg);
        }

    }

}
