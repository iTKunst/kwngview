// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtSvgRawFltrCmp} from "./kwtSvgRawFltrCmp";


const sTAG: string = "cmp";


@Component({
    selector: 'kwc-svg-raw-fltr-cmp',
    templateUrl: 'kwcSvgRawFltrCmp.html',
    styleUrls: ['kwcSvgRawFltrCmp.scss']
})
export class kwcSvgRawFltrCmp extends kwNgCtrlCompId {
    sIn: string;
    sIn2: string;
    sOperator: string;
    sResult: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcSvgRawFltr::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtSvgRawFltrCmp = <kwtSvgRawFltrCmp>view;

        let sIn: string = type.sIn;
        if (kw.isString(sIn)) {
            //console.info(this.sClass, "::parseView() sIn is [", sIn, "]");
            this.sIn = sIn
        }

        let sIn2: string = type.sIn2;
        if (kw.isString(sIn2)) {
            //console.info(this.sClass, "::parseView() sIn2 is [", sIn2, "]");
            this.sIn2 = sIn2
        }

        let sOperator: string = type.sOperator;
        if (kw.isString(sOperator)) {
            //console.info(this.sClass, "::parseView() sOperator is [", sOperator, "]");
            this.sOperator = sOperator
        }

        let sResult: string = type.sResult;
        if (kw.isString(sResult)) {
            //console.info(this.sClass, "::parseView() sResult is [", sResult, "]");
            this.sResult = sResult
        }

        if (!kw.isString(sIn)
            && !kw.isString(sIn2)
            && !kw.isString(sOperator)
            && !kw.isString(sResult)) {
            let sMsg = "Recognizes only [sIn, sIn2, sOperator, sResult]";
            console.error(this.sClass, "::parseView()", sMsg);
        }

    }

}
