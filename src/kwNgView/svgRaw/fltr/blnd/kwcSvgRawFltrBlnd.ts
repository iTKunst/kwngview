// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtSvgRawFltrBlnd} from "./kwtSvgRawFltrBlnd";


const sTAG: string = "blnd";

@Component({
    selector: 'kwc-svg-raw-fltr-blnd',
    templateUrl: 'kwcSvgRawFltrBlnd.html',
    styleUrls: ['kwcSvgRawFltrBlnd.scss']
})
export class kwcSvgRawFltrBlnd extends kwNgCtrlCompId {

    public sIn: string;
    public sIn2: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcSvgRawFltr::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtSvgRawFltrBlnd = <kwtSvgRawFltrBlnd>view;

        let sIn: string = type.sIn;
        if (kw.isString(sIn)) {
            //console.info(this.sClass, "::parseView() sIn is [", sIn, "]");
            this.sIn = sIn
        }

        let sIn2: string = type.sIn2;
        if (kw.isString(sIn2)) {
            //console.info(this.sClass, "::parseView() sIn2 is [", sIn2, "]");
            this.sIn2 = sIn2
        }

        if (!kw.isString(sIn)
            && !kw.isString(sIn2)) {
            let sMsg = "Recognizes only [sIn, sIn2]";
            console.error(this.sClass, "::parseView()", sMsg);
        }

    }


}
