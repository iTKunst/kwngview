// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtSvgRawFltrFld} from "./kwtSvgRawFltrFld";


const sTAG: string = "fld";


@Component({
    selector: 'kwc-svg-raw-fltr-fld',
    templateUrl: 'kwcSvgRawFltrFld.html',
    styleUrls: ['kwcSvgRawFltrFld.scss']
})
export class kwcSvgRawFltrFld extends kwNgCtrlCompId {
    sColor: string;
    sOpacity: string;
    sResult: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcSvgRawFltr::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtSvgRawFltrFld = <kwtSvgRawFltrFld>view;

        let sColor: string = type.sColor;
        if (kw.isString(sColor)) {
            //console.info(this.sClass, "::parseView() sColor is [", sColor, "]");
            this.sColor = sColor
        }

        let sOpacity: string = type.sOpacity;
        if (kw.isString(sOpacity)) {
            //console.info(this.sClass, "::parseView() sOpacity is [", sOpacity, "]");
            this.sOpacity = sOpacity
        }

        let sResult: string = type.sResult;
        if (kw.isString(sResult)) {
            //console.info(this.sClass, "::parseView() sResult is [", sResult, "]");
            this.sResult = sResult
        }

        if (!kw.isString(sColor)
            && !kw.isString(sOpacity)
            && !kw.isString(sResult)) {
            let sMsg = "Recognizes only [sColor, sOpacity, sResult]";
            console.error(this.sClass, "::parseView()", sMsg);
        }

    }

}
