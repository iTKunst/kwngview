// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtSvgRawFltr} from "./kwtSvgRawFltr";


const sTAG: string = "fltr";

const sDEFAULT_FILL_RULE: string = "evenodd";

@Component({
    selector: 'kwc-svg-raw-fltr',
    templateUrl: 'kwcSvgRawFltr.html',
    styleUrls: ['kwcSvgRawFltr.scss']
})
export class kwcSvgRawFltr extends kwNgCtrlCompId {

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcSvgRawFltr::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtSvgRawFltr = <kwtSvgRawFltr>this.view;

        let blnd: object = type.blnd;
        if (kw.isValid(blnd)) {
            //console.info(this.sClass, "::parseView() blnd is [", blnd, "]");
        }

        let cmp: object = type.cmp;
        if (kw.isValid(cmp)) {
            //console.info(this.sClass, "::parseView() cmp is [", cmp, "]");
        }

        let fld: object = type.fld;
        if (kw.isValid(fld)) {
            //console.info(this.sClass, "::parseView() fld is [", fld, "]");
        }

        if (!kw.isValid(blnd)
            && !kw.isValid(cmp)
            && !kw.isValid(fld)) {
            let sMsg = "Recognizes only [blnd, cmp, fld]";
            console.error(this.sClass, "::parseView()", sMsg);
        }


    }

}
