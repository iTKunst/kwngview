// @formatter:off
import {Component} from '@angular/core';

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtSvgRawPath} from "./kwtSvgRawPath";


const sTAG: string = "path";

const sDEFAULT_FILL_RULE: string = "evenodd";

@Component({
    selector: 'kwc-svg-raw-path',
    templateUrl: 'kwcSvgRawPath.html',
    styleUrls: ['kwcSvgRawPath.scss']
})
export class kwcSvgRawPath extends kwNgCtrlCompId {
    sD: string;
    sFill: string;
    sFillRule: string;
    sFilter: string;

    constructor(
        srvcDisp: kwBSDispVal
    ) {
        super(sTAG, srvcDisp);
        //console.log('kwcSvgRawPath::constructor() called.');
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }

    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");

        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtSvgRawPath = <kwtSvgRawPath>this.view;

        let sD: string = type.sD;
        if (kw.isString(sD)) {
            //console.info(this.sClass, "::parseView() sD is [", sD, "]");
            this.sD = sD
        }

        let sFill: string = type.sFill;
        if (kw.isString(sFill)) {
            //console.info(this.sClass, "::parseView() sFill is [", sFill, "]");
            this.sFill = sFill
        }

        let sFillRule: string = type.sFillRule;
        if (kw.isString(sFillRule)) {
            //console.info(this.sClass, "::parseView() sFillRule is [", sFillRule, "]");
            this.sFillRule = sFillRule
        }

        let sFilter: string = type.sFilter;
        if (kw.isString(sFilter)) {
            //console.info(this.sClass, "::parseView() sFilter is [", sFilter, "]");
            this.sFilter = sFilter
        }

        if (!kw.isString(sD)
            && !kw.isString(sFill)
            && !kw.isString(sFillRule)
            && !kw.isString(sFilter)) {
            let sMsg = "Recognizes only [sD, sFill, sFillRule, sFilter]";
            console.error(this.sClass, "::parseView()", sMsg);
        }

    }

}
