/**********************************************************************
 *
 * kwNgComp/bdy/kwcBdyLft.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import {Component} from "@angular/core";

import {kw} from "@kw/kw";
import {kwBSDispVal} from "@kwNgBS/disp/kwBSDispVal";
import {kwNgCtrlCompId} from "@kwNgClass/kwNgCtrlCompId";
import {kwtBdyLft} from "./kwtBdyLft";

// @formatter:on

const sTAG: string = "lft";

@Component({
    selector: "kwc-bdy-lft",
    templateUrl: "kwcBdyLft.html",
    styleUrls: ["kwcBdyLft.scss"]
})
export class kwcBdyLft extends kwNgCtrlCompId {

    nav: object;
    side: object;
    opts: object;
    tabs: object;

    constructor(srvcDisp: kwBSDispVal) {
        super(sTAG, srvcDisp);
        //console.log(this.sClass, "::constructor() called.");
    }

// @formatter:on

    protected initCmp(): void {
        //console.log(this.sClass, "::parseView() called.");
    }


    protected parseData(data: any): void {
        //console.log(this.sClass, "::parseData() called.");
    }

    protected parseInits(inits: object): void {
        //console.log(this.sClass, "::parseInits() called.");
    }

    protected parseView(view: object): void {
        //console.log(this.sClass, "::parseView() called.");


        if (kw.isNull(view)) {
            console.error(this.sClass, "::parseView() view is invalid");
            return;
        }

        let type: kwtBdyLft = <kwtBdyLft>view;

        let nav: object = type.nav;
        if (kw.isValid(nav)) {
            //console.info(this.sClass, "::parseView() lft is [", lft, "]");
            this.nav = nav
        }


        let side: object = type.side;
        if (kw.isValid(side)) {
            //console.info(this.sClass, "::parseView() rht is [", rht, "]");
            this.side = side
        }

        let opts: object = type.opts;
        if (kw.isValid(opts)) {
            //console.info(this.sClass, "::parseView() rht is [", rht, "]");
            this.opts = opts
        }

        let tabs: object = type.tabs;
        if (kw.isValid(side)) {
            //console.info(this.sClass, "::parseView() rht is [", rht, "]");
            this.tabs = tabs
        }


        if (kw.isNull(nav) && kw.isNull(side) && kw.isNull(opts) && kw.isNull(tabs)) {
            console.error(this.sClass, "::parseView() Only recognizes [subPage, nav, side, opts, tabs]");
        }
    }

}
